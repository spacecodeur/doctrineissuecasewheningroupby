# Doctrine 2.12.3 : bug when using `CASE WHEN` with `addGroupBy()`

- [link to the Bug Report (doctrine-orm repo)](https://github.com/doctrine/orm/issues/9856)

## Prerequisites

- install composer ([link](https://getcomposer.org/download/))
- install symfony cli ([link](https://symfony.com/download))

Below, the commands for deploy quicly this project : 

```bash
cd /tmp && \
git clone git@gitlab.com:spacecodeur/doctrineissuecasewheningroupby.git && \
cd doctrineissuecasewheningroupby && \
composer install && \
./bin/console d:d:c && ./bin/console d:s:c && ./bin/console d:f:l -q && \
symfony server:start -d --port=9123
```

Open the URL https://127.0.0.1:9123 with your favorite browser. And then, you'll see the home page where the issue is showed : 

![homepage](https://i.ibb.co/sVxH9gX/Screenshot-from-2022-06-17-22-25-55.png)

And if you click on the link, to the bottom, you'll be redirected to a page using the query using `addGroupBy()` with `CASE WHEN ...` and get the error :

![error](https://i.ibb.co/N9G2Zhj/Screenshot-from-2022-06-17-22-29-41.png)