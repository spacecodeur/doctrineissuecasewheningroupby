<?php

namespace App\DataFixtures;

use App\Entity\Content;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ContentFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // each column of content maybe true, false or null
        $values = [true, false, null];

        // generate all combinaisons possible of contents
        foreach ($values as $i){
            foreach ($values as $j){
                foreach ($values as $k){
                    $manager->persist(new Content($i, $j ,$k));
                }
            }
        }

        $manager->flush();
    }
}
