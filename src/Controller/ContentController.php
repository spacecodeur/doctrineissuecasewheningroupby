<?php

namespace App\Controller;

use App\Entity\Content;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContentController extends AbstractController
{

    private EntityManagerInterface $em;

    /**
     * ContentController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    #[Route('/', name: 'app_content')]
    public function index(): Response
    {
        $alldatas = $this->em->getRepository(Content::class)->findAll();
        $query = $this->em->getRepository(Content::class)->notBuggedFind();

        return $this->render('content/index.html.twig', [
            'allDatas' => $alldatas,
            'query' => $query
        ]);
    }

    #[Route('/contentError', name: 'app_content_error')]
    public function indexError(): Response
    {
        $queryError = $this->em->getRepository(Content::class)->buggedFind();

        return $this->render('content/index.html.twig', [
            'query' => $queryError,
        ]);
    }
}
