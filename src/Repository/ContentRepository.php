<?php

namespace App\Repository;

use App\Entity\Content;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Content>
 *
 * @method Content|null find($id, $lockMode = null, $lockVersion = null)
 * @method Content|null findOneBy(array $criteria, array $orderBy = null)
 * @method Content[]    findAll()
 * @method Content[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Content::class);
    }

    public function add(Content $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Content $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function notBuggedFind(){
        $q = $this->createQueryBuilder('content');

        $q
            ->addSelect("CASE
    WHEN content.col1 IS NOT NULL THEN 'foo'
    WHEN content.col2 IS NOT NULL THEN 'faa'
    ELSE 'fuu'
END AS col4added")

            ->addOrderBy('CASE
    WHEN content.col1 IS NOT NULL THEN content.col1
    WHEN content.col2 IS NOT NULL THEN content.col2
    ELSE content.col3
END','ASC');

        dump($q->getQuery()->getDQL());
        dump($q->getQuery()->getSQL());

        return $q->getQuery()->getResult();
    }

    public function buggedFind(){
        $q = $this->createQueryBuilder('content');

        $q
            ->addSelect("CASE
    WHEN content.col1 IS NOT NULL THEN 'foo'
    WHEN content.col2 IS NOT NULL THEN 'faa'
    ELSE 'fuu'
END AS col4added")

            ->addOrderBy('CASE
    WHEN content.col1 IS NOT NULL THEN content.col1
    WHEN content.col2 IS NOT NULL THEN content.col2
    ELSE content.col3
END','ASC')

            ->addGroupBy('CASE 
    WHEN content.col1 IS NOT NULL THEN content.col1 
    ELSE content.col2
END');

        dump($q->getQuery()->getDQL());
        dump($q->getQuery()->getSQL());

        return $q->getQuery()->getResult();
    }

//    /**
//     * @return Content[] Returns an array of Content objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Content
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
