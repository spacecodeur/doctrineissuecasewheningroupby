<?php

namespace App\Entity;

use App\Repository\ContentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContentRepository::class)]
class Content
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $col1;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $col2;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $col3;

    /**
     * Content constructor.
     * @param $col1
     * @param $col2
     * @param $col3
     */
    public function __construct($col1, $col2, $col3)
    {
        $this->col1 = $col1;
        $this->col2 = $col2;
        $this->col3 = $col3;
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    public function isCol1(): ?bool
    {
        return $this->col1;
    }

    public function setCol1(?bool $col1): self
    {
        $this->col1 = $col1;

        return $this;
    }

    public function isCol2(): ?bool
    {
        return $this->col2;
    }

    public function setCol2(?bool $col2): self
    {
        $this->col2 = $col2;

        return $this;
    }

    public function isCol3(): ?bool
    {
        return $this->col3;
    }

    public function setCol3(?bool $col3): self
    {
        $this->col3 = $col3;

        return $this;
    }
}
